import lombok.Data;

@Data
public class Transport {
    private Boolean isBusy = Boolean.FALSE;
    private Boolean isBackWay = Boolean.FALSE;
    private Integer daysInTrip = 0;
    private Route currentRoute;

}
