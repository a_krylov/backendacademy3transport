import lombok.Data;

@Data
public class Route {
    private PointName pointName1;
    private PointName pointName2;

    public Route(PointName pointName1, PointName pointName2) {
        this.pointName1=pointName1;
        this.pointName2=pointName2;
    }
}
