import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

public class DeliveryFactory {
    static Delivery delivery = new Delivery();

    public static Delivery makeDelivery(List<PointName> pointNames) {
        delivery.setBuses(asList(new Bus(), new Bus()));
        delivery.setShip(new Ship());
        delivery.setPointNamesEnd(pointNames);
        makeRoutesFromPointsEnd(pointNames);
        makePointsFromPointsEnd(pointNames);
        return delivery;
    }

    private static void makeRoutesFromPointsEnd(List<PointName> pointNames) {
        List<Route> routesForBus = new ArrayList<>();
        List<Route> routesForShip = new ArrayList<>();
        for (int i = 0; i < pointNames.size(); i++) {
            if (pointNames.get(i).equals(PointName.A)) {
                routesForBus.add(new Route(PointName.S, pointNames.get(i)));
            }
            if (pointNames.get(i).equals(PointName.B)) {
                routesForBus.add(new Route(PointName.S, PointName.P));
                routesForShip.add(new Route(PointName.P, PointName.B));
            }
        }
        delivery.setRoutesForBus(routesForBus);
        delivery.setRoutesForShip(routesForShip);
    }

    static void makePointsFromPointsEnd(List<PointName> pointNames) {

        Map<PointName, Integer> mapPoint = new HashMap<>();
        mapPoint.putIfAbsent(PointName.S, pointNames.size());
        mapPoint.putIfAbsent(PointName.P, 0);
        mapPoint.putIfAbsent(PointName.A, 0);
        mapPoint.putIfAbsent(PointName.B, 0);

        delivery.setMapPoint(mapPoint);
    }
}
