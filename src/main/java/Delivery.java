import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Delivery {
    private List<PointName> pointNamesEnd;
    private List<Route> routesForBus;
    private List<Route> routesForShip;
    private Map<PointName, Integer> mapPoint = new HashMap<>();
    private List<Bus> buses;
    private Ship ship;

    public Delivery() {
    }

    public Integer prognoz() {
        Integer days = 0;
        while (true) {
            for (int i = 0; i < buses.size(); i++) {
                if (buses.get(i).getIsBusy()) {
                    transportIsBusy(buses.get(i));
                }
                if (!buses.get(i).getIsBusy() && routesForBus.size() != 0) {
                    transportIsNotBusy(buses.get(i), routesForBus, PointName.S);
                }
            }

            if (ship.getIsBusy()) {
                transportIsBusy(ship);
            }
            if (!ship.getIsBusy()
                    && routesForShip.size() != 0
                    && mapPoint.get(PointName.P) > 0) {
                transportIsNotBusy(ship, routesForShip, PointName.P);
            }

            if ((mapPoint.get(PointName.A) + mapPoint.get(PointName.B)) == pointNamesEnd.size()) {
                return days;
            }
            days++;
        }
    }

    private void transportIsBusy(Transport transport) {
        transport.setDaysInTrip(transport.getDaysInTrip() + 1);
        if (transport.getDaysInTrip().equals(
                PointsTime.timeBetweenPoints(
                        transport.getCurrentRoute().getPointName1(),
                        transport.getCurrentRoute().getPointName2()))) {
            if (!transport.getIsBackWay()) {
                mapPoint.put(
                        transport.getCurrentRoute().getPointName2(),
                        mapPoint.get(transport.getCurrentRoute().getPointName2()) + 1);
                transport.setDaysInTrip(0);
                transport.setIsBackWay(Boolean.TRUE);
            } else {
                transport.setDaysInTrip(0);
                transport.setIsBackWay(Boolean.FALSE);
                transport.setIsBusy(Boolean.FALSE);
            }
        }
    }

    private void transportIsNotBusy(Transport transport, List<Route> routes, PointName startPoint) {
        transport.setCurrentRoute(routes.get(0));
        transport.setIsBusy(Boolean.TRUE);
        routes.remove(0);
        mapPoint.put(startPoint, mapPoint.get(startPoint) - 1);
    }
}
