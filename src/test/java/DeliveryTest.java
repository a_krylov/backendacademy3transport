import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeliveryTest {

    @Test
    public void testA() {
        List<PointName> pointNames = singletonList(PointName.A);
        Delivery delivery = DeliveryFactory.makeDelivery(pointNames);
        Integer time = delivery.prognoz();
        assertEquals(5, time);
    }

    @Test
    public void testB() {
        List<PointName> pointNames = singletonList(PointName.B);
        Delivery delivery = DeliveryFactory.makeDelivery(pointNames);
        Integer time = delivery.prognoz();
        assertEquals(5, time);
    }

    @Test
    public void testAA() {
        List<PointName> pointNames = asList(PointName.A, PointName.A);
        Delivery delivery = DeliveryFactory.makeDelivery(pointNames);
        Integer time = delivery.prognoz();
        assertEquals(5, time);
    }

    @Test
    public void testBB() {
        List<PointName> pointNames = asList(PointName.B, PointName.B);
        Delivery delivery = DeliveryFactory.makeDelivery(pointNames);
        Integer time = delivery.prognoz();
        assertEquals(13, time);
    }

    @Test
    public void testAB() {
        List<PointName> pointNames = asList(PointName.A, PointName.B);
        Delivery delivery = DeliveryFactory.makeDelivery(pointNames);
        Integer time = delivery.prognoz();
        assertEquals(5, time);
    }

    @Test
    public void testBA() {
        List<PointName> pointNames = asList(PointName.A, PointName.B);
        Delivery delivery = DeliveryFactory.makeDelivery(pointNames);
        Integer time = delivery.prognoz();
        assertEquals(5, time);
    }

    @Test
    public void testAAA() {
        List<PointName> pointNames = asList(PointName.A, PointName.A, PointName.A);
        Delivery delivery = DeliveryFactory.makeDelivery(pointNames);
        Integer time = delivery.prognoz();
        assertEquals(15, time);
    }

    @Test
    public void testBAA() {
        List<PointName> pointNames = asList(PointName.B, PointName.A, PointName.A);
        Delivery delivery = DeliveryFactory.makeDelivery(pointNames);
        Integer time = delivery.prognoz();
        assertEquals(7, time);
    }

    @Test
    public void testBBABAABA() {
        List<PointName> pointNames = asList(PointName.B, PointName.B, PointName.A, PointName.B, PointName.A, PointName.A, PointName.B, PointName.A);
        Delivery delivery = DeliveryFactory.makeDelivery(pointNames);
        Integer time = delivery.prognoz();
        assertEquals(29, time);
    }

    @Test
    public void testBAAABABBBAAA() {
        List<PointName> pointNames = asList(PointName.B, PointName.A, PointName.A, PointName.A,
                PointName.B, PointName.A, PointName.B, PointName.B,
                PointName.B, PointName.A, PointName.A, PointName.A);
        Delivery delivery = DeliveryFactory.makeDelivery(pointNames);
        Integer time = delivery.prognoz();
        assertEquals(1, time);
    }
}
